#All scopes are country scope
#All parameters are optional

estate_tiger_command_land_rights = {
	icon = privilege_grant_autonomy
	loyalty = 0.05
	influence = 0.05
	land_share = 5
	max_absolutism = -5
	
	can_select = {
	}
	
	on_granted = {
	}
	
	penalties = {
	}
	
	benefits = {
		governing_capacity = 100
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_tiger_command_x = {
	#icon = 
	loyalty = 0.1
	influence = 0.1
	land_share = 0
	max_absolutism = -10
	
	can_select = {
	}
	
	on_granted = {
	}
	
	penalties = {
	}
	
	benefits = {
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_tiger_command_reassess_teachings = {
	#icon = 
	#loyalty = 
	#influence = 
	#land_share = 
	#max_absolutism = 
	
	can_select = {
	}
	
	is_valid = {
		OR = {
			NOT = { has_country_flag = revoke_tiger_command_reasses_teachings }
			NOT = { has_estate_privilege = estate_tiger_command_reasses_teachings }
		}
	}
	
	on_granted = {
		custom_tooltip = tiger_command_reasses_teachings_tt
		country_event = {
			#id = event_id # event that allow you to change deity
		}
	}
	
	penalties = {
	}
	
	benefits = {
	}
	
	cooldown_years = 20 # idk
	
	on_cooldown_expires = {
		set_country_flag = revoke_tiger_command_reasses_teachings
	}
	
	on_invalid = {
		clr_country_flag = revoke_tiger_command_reasses_teachings
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_tiger_command_invite_wuhyun_philosophers = {
	#icon = 
	#loyalty = 
	#influence = 
	#land_share = 
	#max_absolutism = 
	
	can_select = {
	}
	
	is_valid = {
	}
	
	on_granted = {
		custom_tooltip = tiger_command_invite_wuhyun_philosophers_tt
		hidden_effect = {
			country_event = { # event to invite hp dudes
				# id = event_id
			}
		}
	}
	
	penalties = {
	}
	
	benefits = {
	}
	
	cooldown_years = 20 # idk
	
	on_cooldown_expires = {
		set_country_flag = revoke_tiger_command_reasses_teachings
	}
	
	on_invalid = {
		clr_country_flag = revoke_tiger_command_reasses_teachings
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}

estate_tiger_command_broadened_education = {
	#icon = 
	#loyalty = 
	#influence = 
	#land_share = 
	#max_absolutism = 
	
	can_select = {
	}
	
	is_valid = {
	}
	
	on_granted = {
	}
	
	penalties = {
	}
	
	benefits = {
		adm_advisor_cost = -0.25
	}
}

estate_tiger_command_management = {	# this need more details
	#icon = 
	#loyalty = 
	#influence = 
	#land_share = 
	#max_absolutism = 
	
	can_select = {
	}
	
	on_granted = {
		if = {
			limit = {
				any_owned_province = {
					has_province_modifier = hob_tiger_war_camp # make this check teh correct province/modifier
				}
			}
			custom_tooltip = "Add a cool modifier in the Tiger War Camp"
		}
		else = {
			custom_tooltip = "Has no effects upon being granted."
		}
	}
	
	on_granted_province = {
		random_owned_province = {
			limit = { has_province_modifier = hob_tiger_war_camp } # make this check teh correct province/modifier
			custom_tooltip = estate_tiger_command_management_tt
			hidden_effect = { add_province_triggered_modifier = hob_tiger_management }
		}
	}
	
	on_revoked = {
		custom_tooltip = "Remove the cool modifier in the Tiger War Camp"
	}
	
	on_revoked_province = {
		random_owned_province = {
			limit = { has_province_modifier = hob_tiger_management }
			custom_tooltip = revoke_estate_tiger_command_management_tt
			hidden_effect = { remove_province_triggered_modifier = hob_tiger_management }
		}
	}
	
	on_invalid = {
		custom_tooltip = "Remove the cool modifier in the Tiger War Camp"
	}
	
	on_invalid_province = {
		random_owned_province = {
			limit = { has_province_modifier = hob_tiger_management }
			custom_tooltip = revoke_estate_tiger_command_management_tt
			hidden_effect = { remove_province_triggered_modifier = hob_tiger_management }
		}
	}
	
	penalties = {
	}
	
	benefits = {
	}
	
	modifier_by_land_ownership = {
		global_autonomy = -0.1
	}
	
	ai_will_do = {
		#factor = x
		#triggers
	}
}
